# Определение нужных модулей (Таланов Денис)
import threading
import time
from socket import gethostbyname, gethostname, socket, AF_INET, SOCK_DGRAM
import os
from Bot_Luke import work_bot
import random
import io

# Реализации функции принимающей все данные и шифрование сообщений на сервере (Катя Лямзина)
# Ключ для шифровки данных
key = 8194
shutdown = False
join = False


# Принятие данных другого пользователя
def receving(name, sock):
    while not shutdown:
        try:
            while True:
                data, addr = sock.recvfrom(1024)
                # print(data.decode("utf-8"))

                # Begin
                decrypt = "";
                k = False
                for i in data.decode("utf-8"):
                    if i == ":":
                        k = True
                        decrypt += i
                    elif k == False or i == " ":
                        decrypt += i
                    else:
                        decrypt += chr(ord(i) ^ key)
                print(decrypt)
                # End
                time.sleep(0.2)
        except:
            pass


# Может принимать Ip разных пользователей, порт = 0, так как он не создаёт сеть, а только использует
host = gethostbyname(gethostname())
port = 0
Server = (host,10033)

# Поднятие протоколов UDP и IP
s = socket(AF_INET, SOCK_DGRAM)

# Хост который использовали, а порт сервер распределит сам
s.bind((host, port))

# Неблокирующийрежим(справляет ошибки выхода из чата)
s.setblocking(0)


#------------------------------------------------------------------------------------------------------
# Валидация имени пользователя лаба 4(2,4 фичи)
with open('users.txt','a') as file:
        pass
def username():
    while True:
        new_alias = input("Name:")
        chars = set(",/|\[]{}.!><1234567890#№@&^+=-")

        #-------------------------------------------------
        # Лаба 4 (3) Уникальность имени  
        if os.path.getsize('users.txt') != 0:
            chois = True
            with open('users.txt','r') as f:
                for nickname in f:
                    if nickname == new_alias + "\n":
                        chois = False
                        break
                    else:
                        continue
            if chois == False:
                print("Имя не уникально")
                chois = True
                continue
            else:
                print("Имя уникально")        
        else:
            print("Имя уникально")
        #--------------------------------------------------

        if len(new_alias) > 255:
            print("Кол-во символов превосходит допустимое")
            continue
        elif any ((c in chars) for c in new_alias):
            print("Имя не должно содержать следующих символов -> ' ,/|\[]{}.!><1234567890#№@&^+_=-'")
            continue    
        else:
            break
    return new_alias
#-------------------------------------------------------------------------------------------------------
alias = username()


with open('users.txt','a') as file:
    file.write(alias + '\n')

# Многопоточность (отправление сообщений параллельно нескольких пользователей в одно время)
rT = threading.Thread(target=receving, args=("RecvThread", s))
rT.start()


# Пока клиент в сети может отправлять сообщения, если вышел уведомить сервер, реализвать деятельность клиентов
while shutdown == False:
    if join == False:
        # Отправление сообщения на сервер о подключении клиента
        s.sendto(("[" + alias + " ] => join chat ").encode("utf-8"), Server)
        join = True
    else:
        # Проверка подключения клиента к чату
        with open('users.txt', 'r') as f:
            name_nic = f.readlines()
        if alias + "\n" not in name_nic:
            print('Вас удалили, вы больше не можете отправлять сообщения другим участникам чата!')
            s.sendto(("[" + alias + "] <= left chat ").encode("utf-8"), Server)
            del f
            shutdown = True

        # Сообщение клиента    
        message = input()

        #----------------------------------------------------------------------------------
        # Режим админа
        if message == "/admin":
            stop = True
            while stop:
                admin = input("Для включения режима admin введите пароль: ")
                if admin == "admin777":
                    s.sendto(("[" + alias + "] changed username to [ Admin ]").encode("utf-8"), Server)
                    print("Вы вошли в режим админа")
                    stop_admin = True
                    while stop_admin:
                        request = input('New command: ')
                        # Команды
                        # Кол-во клиентов 
                        if request == "/count_client":
                            with open('users.txt','r') as fi:
                                lines = fi.readlines()
                            print("Кол-во пользователей: ", len(lines))
                            continue
                        # Удаление клиента
                        elif request == "/delete_cl":
                            del_cl = input("Введите имя клиента, которого хотите удалить: ") + "\n"

                            with open('users.txt','r') as file:
                                lines = file.readlines()
                            for li in lines:
                                if del_cl == li:
                                    lines.remove(li)

                            os.remove('users.txt')

                            with open('users.txt','a') as f:
                                for line in lines:
                                    f.write(line)
                                    continue
                        elif request == "exit":
                            print("Вы вышли из режима admin!")
                            s.sendto(("[ Admin ] changed username to [ " + alias + "]").encode("utf-8"), Server)
                            stop_admin = False
                            stop = False
                else:
                    print("Пароль не верный!")
                    continue


        #----------------------------------------------------------------------------------
        
        #----------------------------------------------------------------------------------
        # Призыв Бота Люк
        if message == "Бот приди":
            s.sendto(("[ Bot Luke ]*ghost => join chat ").encode("utf-8"), Server)
            time.sleep(0.2)
            print("Я тут!")
            while True:
                message = input()

                bye = ['exit','Пока','пока','всего хорошего', 'Всего хорошего', 'Пока бот','Прощай','Досвидания','До свидания']
                spec_instrac = ['/count_clients','/status']

                if message in bye:
                    random_phrases = ['Пока','Давай до свидания!', 'Ещё увидимся', 'ВсеГо, ХорошеГо!']
                    s.sendto(("[ Bot Luke ]*ghost <= left chat ").encode("utf-8"), Server)
                    print(random.choice(random_phrases))
                    if os.path.exists('questions.txt') == True:
                        os.path.exists('questions.txt')
                        os.remove("questions.txt")
                    break
                elif message in spec_instrac:
                    if message == '/count_clients':
                        with open('users.txt','r') as fi:
                            lines = fi.readlines()
                        print("Кол-во пользователей: ", len(lines))
                elif message not in bye:
                    mes = message.replace(' ','')
                    mes = mes.lower()
                    mes = mes + " "
                    with open('questions.txt','a') as txt:
                        txt.write(mes)
                    message_bot = work_bot()
        #------------------------------------------------------------------------------------

        # Изменение имени пользователя лаба 4(6 фича)
        # ------------------------------------------------------------------------------------
        # Изменение имени пользователя
        if message == "\change_username":
            with open('users.txt','r') as file:
                lines = file.readlines()
            os.remove('users.txt')

            with open('users.txt','a') as f:
                for line in lines:
                    if line != alias +"\n":
                        f.write(line)

            message = username()
            with open('users.txt','a') as file:
                file.write(message + '\n')

            s.sendto(("[" + alias + "] changed username to [" + message + "]").encode("utf-8"), Server)
            alias = message
            continue
        # ------------------------------------------------------------------------------------
        #Выход из чата
        if message == "Выход":
            
            s.sendto(("[" + alias + "] <= left chat ").encode("utf-8"), Server)
            
            with open('users.txt','r') as file:
                lines = file.readlines()
            os.remove('users.txt')
            with open('users.txt','a') as f:
                for line in lines:
                    if line != alias +"\n":
                        f.write(line)
            
            shutdown = True
        else:
            # Begin кодировка сообщения на сервере
            crypt = ""
            for i in message:
                crypt += chr(ord(i) ^ key)
            message = crypt
            # End

            # Отправка сообщения клиента на сервер
            if message != "":
                s.sendto(("[" + alias + "] :: " + message).encode("utf-8"), Server)

            time.sleep(0.2)
rT.join()
s.close()
