import random

def work_bot():
    que = []
    with open('questions.txt','r') as fi:
        for question in fi:
            que.append(question)
            que = que[0].split(" ")

        questions = que[len(que)-2]

        BOT_DICTIONARY = {

            'intents': {
                'hello':{
                    'examples':['привет','добрыйдень','шалом','саламалейкум','приветбратишка','приветлюк'],
                    'responses':['Привет, человек','Здорова, кожаный мешок','Салют','Здравствуй','Шалом','Алейкум Ассалам']
                },
                'how_are_you':{
                    'examples':['какдела?','какдела','какпоживаешь','какпоживаешь?','какжитухабро','какжизнь','какжизнь','какжизнь?'],
                    'responses':['Да всё круто, человек','Всё хорошо!','Всё отлично, как у тебя?','Пойдёёёёёёт','Хочу отдохнуть(','Всё найс','Всё бомба','Nice']
                }
            },

            'failure_phrases': [
                'Попробуйте написать по другому', 
                'Что-то не понятно',
                'Я же всего лишь БОТ. Сформулируйте проще!'
            ]
        }

        def get_intent(quest):
            for intent, intent_data in BOT_DICTIONARY['intents'].items():
                for example in intent_data['examples']:
                    if example == quest:
                        return intent
        
        def  get_answer_by_intent(intent):
            if intent in BOT_DICTIONARY['intents']:
                phrases = BOT_DICTIONARY['intents'][intent]['responses']
                return random.choice(phrases)

        def generate_answer_by_text(quest):
            return 
        
        def get_failure_phrases():
            phrases = BOT_DICTIONARY['failure_phrases']
            return random.choice(phrases)


        def answer(quest):
            # NLU
            intent = get_intent(quest)

            # Получение ответа

            # Ищем готовый ответ
            if intent:
                answer = get_answer_by_intent(intent)
                if answer:
                    return answer
            # Генерация подхдящего ответа
            answer = generate_answer_by_text(quest)
            if answer:
                return answer

            # Заглушки
            answer = get_failure_phrases()
            return answer
 
        answer = answer(questions)
        print(answer) 
    del que   
    return answer
#an = work_bot()
