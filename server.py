# Импорт нужных модулей (Дима Зянкин)
import time
import ipaddress
from socket import gethostbyname, gethostname, socket, AF_INET, SOCK_DGRAM
import os
import logging

# Объявление сокетов и протокола UDP для работы сервера (Карина Трофимова)
# Переменные принимающие в себе IP и свободный порт
host = gethostbyname(gethostname())
port = 10033

# Использование семейства сетевых сокетов и протокол UDP
s = socket(AF_INET, SOCK_DGRAM)

# Создание хоста и порта
s.bind((host, port))


# Список клиентов
clients = []
list_client = clients


quit = False
print("[ Server Started ]")

#--------------------------------------------------------------------------------------------
# Лаба 4(5)
#Логгинг сервера config --global user.name "Ni^Z^Z^Z config --global user.email email@mail.ru
logging.basicConfig(filename="sample.log", level=logging.INFO)

logging.debug("This is debug message")
logging.info("Informational message")
logging.error("An error has happened!")
#--------------------------------------------------------------------------------------------



# Работа самого сервера 
while not quit:
    # Приём и отправка пакетов клиентам
    try:
        # Data - сообщение, addr - личный адрес пользователя
        data, addr = s.recvfrom(1024)

        # Добавление клиентов
        if addr not in list_client:
            list_client.append(addr)
            s_tr = data.decode("utf-8")
            name = s_tr[1:-15]
            #-------------------------------------------------------------
            # Кол-во клиентов лаба 4(1)
            with open('users.txt','r') as file:
                lines = file.readlines()
            print("Кол-во пользователей: ", len(lines))
            #-------------------------------------------------------------
            
        # Текущее время добавления клиента
        itsatime = time.strftime("%Y-%m-%d-%H.%M.%S", time.localtime())
        
        print("[" + addr[0] + "]=[" + str(addr[1]) + "]=[" + itsatime + "]/", end="")

        # Перевод кодировки в байты
        print(data.decode("utf-8"))

        # Отправка сообщения и имени клиента другим участникам чата, но не себе
        for client in list_client:
            if addr != client:
                s.sendto(data, client)
    # Если сервер выводит ошибку останавливаем сервер (UDP и IP не используются)
   
    except:
        del list_client
        print("\n[ Server Stopped ]")
        quit = True
  
s.close()
